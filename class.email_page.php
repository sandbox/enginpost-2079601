<?php
/**
 *  Class to handle cURLing Web pages and sending them as formatted HTML in Drupal
 */
global $base_url;

class emailPage{
  private $base_url;
  private $emailTo;
  private $emailFrom;
  private $pageURL;

  public function __get($property) {
    if (property_exists($this, $property)) {
      return $this->$property;
    }
  }

  public function __set($property, $value) {
    if (property_exists($this, $property)) {
      $this->$property = $value;
    }
    return $this;
  }

  public function emailPage( $thisEmailTo = '', $thisEmailFrom = '', $thisPageURL = '' ){
    if( $thisEmailTo != ''){ $this->emailTo = $thisEmailTo; }
    if( $thisEmailFrom != ''){ $this->emailFrom = $thisEmailFrom; }
    if( $thisPageURL != ''){ $this->pageURL = $thisPageURL; }
    $this->_emailPage( $this->_getPage() );
  }

  private function _getPage(){
    $pageHTML = '';
    $ch = curl_init($GLOBALS['base_url'] . "/" . $this->pageURL);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
    $pageHTML = curl_exec($ch);
    curl_close($ch);
    return $pageHTML;
  }

  private function _emailPage( $thisHTML = '' ){
    if( strlen( $thisHTML ) > 0 ){
      $email_system = drupal_mail_system('email_page', 'key');
      $email_message = drupal_mail( 'email_page', 'key', $this->emailTo, language_default(), array(), $this->emailFrom, FALSE);
      $email_message['headers']['Content-Type'] = 'text/html; charset=UTF-8; format=flowed';
      $email_message['body'] = $thisHTML;
      $email_message['subject'] = 'Page from... ' . $GLOBALS['base_url'] . "/" . drupal_get_path_alias( $_GET['q'] );
      $message_result = $email_system->mail($email_message);
      if( $message_result == 1 ){
        drupal_set_message(t('This page has been sent! If the recipient cannot see the message, check spam filters.'));
      }else{
        drupal_set_message(t('There was a problem sending the message. Please, check back later.'));
      }
    }
  }
}
