<?php

/**
 * @file
 *  Admin page callbacks for the email page module.
 */

/**
 *  Form builder. Configure email_page settings.
 *
 *  email_page_settings
 */
function email_page_settings(){
  // Get a list of all enabled site theme options
  $options = get_theme_options();
  // Build the admin form
  $form['email_page_theme'] = array(
    '#title' => t('Email presentation theme'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => variable_get('email_page_theme',array('bartik') ),
    '#description' => t('Users receive an e-mail version of the selected page using the selected theme')
  );
  $form['#submit'][] = 'email_page_theme_submit';
  return system_settings_form( $form );
}

/**
 * Process email_page_settings form submits
 */
function email_page_theme_submit($form, $form_state ){
 // drupal_set_message( "<pre>" . print_r( $form_state, true) . "</pre>"  );
 variable_set('email_page_theme',$form_state['values']['email_page_theme']);
 drupal_set_message('Your email theme is now... ' . $form_state['values']['email_page_theme'] );
}

/**
 *  Form builder. Configure email_page settings for content types.
 *
 *  email_page_settings
 */
function email_page_content_type_settings($form, $form_state, $content_type_name ){
  // Get a list of all enabled site theme options
  $options = get_theme_options();
  // Build the admin form
  $form['email_page_' . $content_type_name . '_theme'] = array(
    '#title' => t('Email presentation theme'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => variable_get('email_page_' . $content_type_name . '_theme',variable_get('email_page_theme',array('bartik') ) ),
    '#description' => t('Users receive an e-mail version of the selected page using the selected theme')
  );
  $form['email_page_variable'] = array(
    '#title' => 'email page variable',
    '#type' => 'hidden',
    '#value' => 'email_page_' . $content_type_name . '_theme'
  );
  $form['#submit'][] = 'email_page_content_type_theme_submit';
  return system_settings_form( $form );
}

/**
 * Process email_page_content_type_settings form submits
 */
function email_page_content_type_theme_submit( $form, $form_state){
  // drupal_set_message( "<pre>" . print_r( $form_state, true) . "</pre>"  );
  variable_set($form_state['values']['email_page_variable'], $form_state['values'][$form_state['values']['email_page_variable']]);
  drupal_set_message('This content type\'s email theme is now... ' . $form_state['values'][$form_state['values']['email_page_variable']] );
}

/**
 * Function returns enabled themes
 */
function get_theme_options(){
  $all_themes = list_themes();
  foreach( $all_themes as $theme ){
    if( $theme->status == 1 ){
      $options[$theme->name] = $theme->name;
    }
  }
  return $options;
}
